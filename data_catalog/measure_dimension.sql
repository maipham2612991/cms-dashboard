CREATE EXTERNAL TABLE IF NOT EXISTS `cms_for_app`.`measure_dimension` (
  `measure_id` string,
  `condition` string,
  `measure_name` string
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe'
STORED AS INPUTFORMAT 'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat' OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
LOCATION 's3://cms-data-for-app/timely_and_effective_care_hospital/star_schema_transformed/measure_dimension/'
TBLPROPERTIES ('classification' = 'parquet');