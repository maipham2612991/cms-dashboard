CREATE EXTERNAL TABLE IF NOT EXISTS `cms_for_app`.`timely_and_effective_care_fact` (
  `facility_id` string,
  `measure_id` string,
  `score` string,
  `sample` int,
  `footnote` int,
  `start_date` string,
  `end_date` string
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe'
STORED AS INPUTFORMAT 'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat' OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
LOCATION 's3://cms-data-for-app/timely_and_effective_care_hospital/star_schema_transformed/timely_and_effective_care_fact/'
TBLPROPERTIES ('classification' = 'parquet');