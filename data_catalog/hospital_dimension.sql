CREATE EXTERNAL TABLE IF NOT EXISTS `cms_for_app`.`hospital_dimension` (
  `facility_id` string,
  `facility_name` string,
  `address` string,
  `city_town` string,
  `state` string,
  `zip_code` int,
  `county_parish` string,
  `telephone_number` string
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe'
STORED AS INPUTFORMAT 'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat' OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
LOCATION 's3://cms-data-for-app/timely_and_effective_care_hospital/star_schema_transformed/hospital_dimension/'
TBLPROPERTIES ('classification' = 'parquet');