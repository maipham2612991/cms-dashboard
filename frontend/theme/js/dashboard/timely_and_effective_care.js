

(function($) {
    $.ajax({
        url: prefixUrl + "/summary",
        contentType: "application/json",
        data: body,
        dataType: "json",
        type: 'GET',
        success: function (response) {
            const data = response.data[0]

            const periodStart = new Date(data.period_start).toLocaleString('en-US', { year: 'numeric', month: 'short'})
            const periodEnd = new Date(data.period_end).toLocaleString('en-US', { year: 'numeric', month: 'short'})
            $('#summary_1').html(
                '<h3 class="card-title text-white">Approx Total Samples</h3> \n' +
                '<div class="d-inline-block"> \n' +
                    '<h2 class="text-white">' + data.total_cares.toLocaleString() + '</h2> \n ' +
                    '<p class="text-white mb-0">' + periodStart + ' to ' + periodEnd + '</p> \n' +
                '</div> \n' +
                '<span class="float-right display-5 opacity-5"><i class="fa fa-medkit"></i></span>'
            )
            $('#summary_2').html(
                '<h3 class="card-title text-white">Total Hospitals</h3> \n' +
                '<div class="d-inline-block"> \n' +
                    '<h2 class="text-white">' + data.total_hospitals.toLocaleString() + '</h2> \n ' +
                    '<p class="text-white mb-0">' + periodStart + ' to ' + periodEnd + '</p> \n' +
                '</div> \n' +
                '<span class="float-right display-5 opacity-5"><i class="fa fa-hospital-o"></i></span>'
            )
            $('#summary_3').html(
                '<h3 class="card-title text-white">Total Condition</h3> \n' +
                '<div class="d-inline-block"> \n' +
                    '<h2 class="text-white">' + data.total_condition + '</h2> \n ' +
                    '<p class="text-white mb-0">' + periodStart + ' to ' + periodEnd + '</p> \n' +
                '</div> \n' +
                '<span class="float-right display-5 opacity-5"><i class="fa fa-calendar-check-o"></i></span>'
            )
            $('#summary_4').html(
                '<h3 class="card-title text-white">Total Measures</h3> \n' +
                '<div class="d-inline-block"> \n' +
                    '<h2 class="text-white">' + data.total_measures + '</h2> \n ' +
                    '<p class="text-white mb-0">' + periodStart + ' to ' + periodEnd + '</p> \n' +
                '</div> \n' +
                '<span class="float-right display-5 opacity-5"><i class="fa fa-heartbeat"></i></span>'
            )
        }
    })
})(jQuery);


displayConditionDetail = function(conditionStats, selected) {
    // Show description on condition
    $.each(conditionStats, function(i, value) {
        if (value.condition === selected) {
            // Update description
            $("#condition-select p").html(
                'Total ' + value.condition + 
                ' Samples: ' + value.total_samples.toLocaleString() +
                ' (' + value.percentage.toFixed(4) + '%)'
            )
        }
    })

};

(function($) {
    $.ajax({
        url: prefixUrl + "/getSamplesByCondition",
        data: body,
        dataType: "json",
        type: 'GET',
        success: function (response) {
            const data = response.data

            // Bar Chart
            new Chartist.Bar('#condition-bar-chart', {
                labels: data.map(item => item.condition),
                series: [
                    data.map(item => ({
                        meta: "Samples: " + item.total_samples.toLocaleString(), 
                        value: item.percentage.toPrecision(4)
                    }))
                ]
                }, {
                seriesBarDistance: 20,
                horizontalBars: true,
                axisY: {
                    offset: 200
                },
                axisX: {
                    labelInterpolationFnc: function(value) {
                        return value + '%';
                    },
                    onlyInteger: true
                },
                plugins: [
                    Chartist.plugins.tooltip()
                ]
            });


            new Chartist.Bar('#hospitals-condition-bar-chart', {
                labels: data.map(item => item.condition),
                series: [
                    data.map(item => ({
                        // meta: "Number of hospitals on this condition: " + item.total_facilities, 
                        value: item.total_facilities
                    }))
                ]
                }, {
                    seriesBarDistance: 10,
                    horizontalBars: false,
                    axisX: {
                      offset: 50
                    },
                    axisY: {
                      offset: 50,
                      scaleMinSpace: 15
                    },
                    plugins: [
                      Chartist.plugins.tooltip()
                    ]
                });


            
            // Select 
            $.each(data, function(i, value) {
                $('#condition-select select').append(
                    '<option value="' + value.condition + '">' + value.condition + '</option>'
                )
            })
            const initSelected = $('#condition-select select').find(":selected").val()
            displayConditionDetail(data, initSelected);
            

            // on Change
            $('#condition-select select').on('change', function() {
                const selected = this.value
                displayConditionDetail(data, selected)
            })
        }
    });

})(jQuery);


displayMeasureDetail = function(data, selected, chart) {
    if (selected !== undefined) {
        const selectedCondition = data.filter((item) => {return item.condition === selected})

        chart.data.datasets[0].data = selectedCondition.map(item => (item.percentage.toFixed(2)))
        chart.data.labels = selectedCondition.map(item => (item.measure_id))
        chart.update()
        
        $("#measure-detail-title").html("Measure Detail of Condition " + selected) 
        $("#measure").DataTable({
            data: selectedCondition,
            "bDestroy": true,
            pageLength : 5,
            "lengthMenu": [[5, 10, 15], [5, 10, 15]],
            columns: [
                { data: "measure_id" },
                { data: "measure_name" },
                { data: "total_samples",
                    "render": function (value) { 
                    if (value === null) return "";
                    return value.toLocaleString()
                    }}     
            ]
        });
    }
    

};

function waitForSelectedValue(selector, callback) {
    var interval = setInterval(function() {
        var selectedValue = $(selector).find(":selected").val();
        if (selectedValue !== undefined) {
            clearInterval(interval);
            callback(selectedValue);
        }
    }, 100); // Check every 100 milliseconds
}


const measureExplain = {
    OP_31: "The purpose of cataract surgery is to improve vision and the quality of life for people with cataracts. This measure shows the percentage of patients aged 18 years and older who had cataract surgery and had improved vision within 90 days following the cataract surgery. Higher percentages are better.",
    OP_29: "This measure shows the percentage of patients aged 50 to 75 years whose colonoscopy did not require removal of a polyp or a biopsy and who received a recommendation for having their next follow-up colonoscopy in 10 years. Individuals between the ages of 50 and 75 who are not at high risk should have a screening colonoscopy every 10 years. Higher percentages are better.",
    OP_22: "This measure shows the percentage of all individuals who signed into an emergency department but left before being evaluated by a healthcare professional. Hospital emergency departments that have high percentages of patients who leave without being seen may not have the staff or resources to provide timely and effective emergency room care. Patients who leave the emergency department without being seen may be seriously ill, putting themselves at higher risk for poor health outcomes. Lower percentages are better.",
    OP_23: "Percentage of patients who came to the emergency department with stroke symptoms who received brain scan results within 45 minutes of arrival. People who suffer from strokes need to get treatment immediately to lessen the amount of brain damage that occurs. A scan of the brain must be taken to determine the type and severity of the stroke before treatment can be provided. Long waits may be a sign that the emergency department is understaffed or overcrowded. Delaying the diagnosis and treatment of strokes may lead to further brain damage. Higher percentages are better.",
    OP_18b: "This measure shows the average (median) time in minutes that patients spent in the emergency department – from the time they arrived, to the time they left. Long stays in the emergency department before a patient leaves may be a sign that the emergency department is understaffed or overcrowded. This may result in delays in treatment, increased suffering for those who wait, and unpleasant treatment environments",
    OP_18c: "This measure shows the average (median) time in minutes that patients spent in the emergency department – from the time they arrived, to the time they left. Long stays in the emergency department before a patient leaves may be a sign that the emergency department is understaffed or overcrowded. This may result in delays in treatment, increased suffering for those who wait, and unpleasant treatment environments",
    ED_2_Strata_1: "Average (Median) time (in minutes) from admit decision time to time of departure from the emergency department for emergency department patients admitted to inpatient status. Reducing the time patients remain in the emergency department (ED) can improve access to treatment and quality of care. ED overcrowding contributes to poor patient outcomes; increased mortality; delayed assessment and care; increased inpatient length of stay; risk of readmission; reduced satisfaction; and exposure to error. Lower numbers are better.",
    ED_2_Strata_2: "Average (Median) time (in minutes) from admit decision time to time of departure from the emergency department for emergency department patients admitted to inpatient status. Reducing the time patients remain in the emergency department (ED) can improve access to treatment and quality of care. ED overcrowding contributes to poor patient outcomes; increased mortality; delayed assessment and care; increased inpatient length of stay; risk of readmission; reduced satisfaction; and exposure to error. Lower numbers are better.",
    OP_3b: "If a hospital doesn't have the facilities to provide specialized heart attack care, it transfers patients who are having a possible heart attack to another hospital that can give them this care. This measure (in minute) shows the average length of time it takes for hospitals to identify patients who need specialized heart attack care the hospital cannot provide and begin their transfer to another hospital. Lower numbers are better.",
    OP_2: "Percentage of outpatients with chest pain or possible heart attack who got drugs to break up blood clots within 30 minutes of arrival. Blood clots can cause heart attacks. Certain patients who are having a heart attack should get a \"clot busting,\" or fibrinolytic, drug to help break up blood clots in blood vessels and improve blood flow to the heart. Standards of care say that patients should get these drugs within 30 minutes of arrival at the hospital. This measure shows what percentage of patients got fibrinolytic drugs within this time. Higher percentages are better.",
    HCP_COVID_19: "CMS believes it is important to incentivize and track healthcare personnel (HCP) vaccination in acute care facilities through quality measurement to protect health care workers, patients, and caregivers, and to help sustain the ability of hospitals to continue serving their communities throughout the Public Health Emergency (PHE) and beyond. The measure will assess the proportion of a hospital’s health care workforce that is vaccinated against COVID-19. Higher percentages are better.",
    IMM_3: "Influenza, or the \"flu,\" is a respiratory illness that is caused by flu viruses and easily spread from person to person. Hospital staff and healthcare workers who are infected with the flu virus can transmit the virus to coworkers and patients, including those at higher risk for getting very sick from the flu. Vaccinating healthcare workers has been found to reduce the risk of flu illness, medical visits, antibiotic use, and flu-related deaths. Higher percentages are better.",
    SAFE_USE_OF_OPIOIDS: "Proportion of inpatient hospitalizations for patients 18 years of age and older prescribed or continued, 2 or more opioids or an opioid and benzodiazepine concurrently at discharge. Unintentional opioid overdose fatalities have become a major public health concern in the U.S. Reducing the number of unintentional overdoses has become a priority for numerous federal organizations. Lower percentages are better.",
    SEP_1: "Percentage of patients who received appropriate care for severe sepsis and septic shock. Best practice guidelines show that early identification of sepsis and early appropriate care can lower the risk of death from sepsis. Higher percentages are better.",
    STK_02: "Ischemic stroke patients prescribed or continuing to take antithrombotic therapy at hospital discharge. The effectiveness of antithrombotic agents in reducing stroke mortality, stroke-related morbidity and recurrence rates has been studied in several large clinical trials. Higher percentages are better.",
    STK_03: "Ischemic stroke patients with atrial fibrillation/flutter who are prescribed or continuing to take anticoagulation therapy at hospital discharge. Atrial fibrillation is a common arrhythmia and an important risk factor for stroke. It is one of several conditions and lifestyle factors that have been identified as risk factors for stroke. Higher percentages are better.",
    STK_05: "Ischemic stroke patients administered antithrombotic therapy by the end of the hospital day 2. Data at this time suggest that antithrombotic therapy should be administered within 2 days of symptom onset in acute ischemic stroke patients to reduce stroke mortality and morbidity as long as no contraindications exist. Higher percentages are better.",
    STK_06: "There is an extensive and consistent body of evidence supporting the use of statins for secondary prevention in patients with clinically evident atherosclerotic cardiovascular disease (ASCVD). High-intensity statin therapy should be initiated or continued as first-line therapy in women and men less than or equal to 75 years of age who have clinical ASCVD, unless contraindicated. In patients with clinical ASCVD and a contraindication to high-intensity statin therapy, moderate-intensity therapy should be considered as an alternative if it can be tolerated. In individuals greater than 75 years of age, the potential for ASCVD risk reduction benefits, adverse effects, drug-drug interactions, and patient preferences should be considered, and statin therapy individualized based on these considerations. Higher percentages are better.",
    VTE_1: "This measure assesses the number of patients who received venous thromboembolism (VTE) prophylaxis or have documentation why no VTE prophylaxis was given the day of or the day after hospital admission or surgery end date for surgeries that start the day of or the day after hospital admission. Hospitalized patients at high-risk for VTE may develop an asymptomatic deep vein thrombosis (DVT) and die from pulmonary embolism (PE) even before the diagnosis is suspected. The majority of fatal events occur as sudden or abrupt death, underscoring the importance of prevention as the most critical action step for reducing death from PE. Higher percentages are better.",
    VTE_2: "This measure assesses the number of patients who received venous thromboembolism (VTE) prophylaxis or have documentation why no VTE prophylaxis was given the day of or the day after the initial admission (or transfer) to the intensive care unit (ICU) or surgery end date for surgeries that start the day of or after the ICU admission (or transfer). Approximately two-thirds of cases of deep vein thrombosis (DVT) or pulmonary emboli (PE) are associated with recent hospitalization. The Agency for Healthcare Research and Quality reports that \"the appropriate application of effective preventive measures in hospitals has major potential for improving patient safety, by reducing the incidence of VTE.\" Higher percentages are better."   
};


(function($) {
    $.ajax({
        url: prefixUrl + "/getSamplesByMeasure",
        data: body,
        dataType: "json",
        type: 'GET',
        success: function (response) {
            const data = response.data

            // Display based on select condition
            ctx = document.getElementById("measure-pie-chart")
            // ctx.height = "200"
            var chart = new Chart(ctx, {
                type: 'pie',
                data: {
                    datasets: [{
                        data: [],
                        backgroundColor: Object.values({
                            red: 'rgb(255, 99, 132)',
                            orange: 'rgb(255, 159, 64)',
                            yellow: 'rgb(255, 205, 86)',
                            green: 'rgb(75, 192, 192)',
                            blue: 'rgb(54, 162, 235)',
                            purple: 'rgb(153, 102, 255)',
                            grey: 'rgb(201, 203, 207)'
                          }),
                        hoverBackgroundColor: Object.values({
                            red: 'rgb(255, 99, 132)',
                            orange: 'rgb(255, 159, 64)',
                            yellow: 'rgb(255, 205, 86)',
                            green: 'rgb(75, 192, 192)',
                            blue: 'rgb(54, 162, 235)',
                            purple: 'rgb(153, 102, 255)',
                            grey: 'rgb(201, 203, 207)'
                          })

                    }],
                    labels: []
                },
                options: {
                    responsive: false,
                    legend: {
                        display: true,
                        position: 'right'
                    }
                }
            });

            waitForSelectedValue('#condition-select select', function(initSelected) {
                displayMeasureDetail(data, initSelected, chart);
            })
            

            // on Change
            $('#condition-select select').on('change', function() {
                displayMeasureDetail(data, this.value, chart)
            })


            $.each(data, function(i, value) {
                $('#measure-select select').append(
                    '<option value="' + value.measure_id + '">' + value.measure_id + '</option>'
                )
            })

            var initMeasure = $('#measure-select select').find(":selected").val()
            const selectedMeasure = data.filter(item => item.measure_id === initMeasure)[0]
            $("#measure-select p").html(
                'Measure Desciption: ' + selectedMeasure.measure_name +
                (measureExplain[selectedMeasure.measure_id] === undefined ? "" : ("<br>" + measureExplain[selectedMeasure.measure_id]))
            )

            $('#measure-select select').on('change', function() {
                const selectedMeasure = data.filter(item => item.measure_id === this.value)[0]
                $("#measure-select p").html(
                    'Measure Desciption: ' + selectedMeasure.measure_name +
                    (measureExplain[selectedMeasure.measure_id] === undefined ? "" : ("<br>" + measureExplain[selectedMeasure.measure_id]))
                )
            })

            
            
        }
    });

})(jQuery);


displayMeasureStats = function(selected, chart) {
    if (selected !== undefined) {
        $.ajax({
            url: prefixUrl + "/getScoreStatsByMeasure",
            data: {"measure_id": selected},
            dataType: "json",
            type: 'GET',
            success: function (response) {
                const data = response.data
                var histDict = {}
                data.forEach(item => histDict[item.score] = item.total_samples)
    
                const scores = data.map(item => parseInt(item.score))
                                    .sort(function(a, b) { return a - b; })
                const minScore =  scores[0]
                const maxScore =  scores[scores.length - 1] > scores[scores.length - 2]*2 ? scores[scores.length - 2] : scores[scores.length - 1]
    
                const labels = Array.from({length: maxScore - minScore + 1}, (_, i) => i + minScore)
    
                chart.data.datasets[0].data = labels.map(i => histDict[i] === undefined ? 0 : histDict[i])
                chart.data.labels = labels
                chart.update()
    
                // $("#hospital-score").DataTable({
                //     "bDestroy": true,
                //     data: data,
                //     columns: [
                //         { data: "facility_name" },
                //         { data: "average_score" }           
                //     ],
                // })
            }
        })
    }

};



(function($) {
    var ctx = document.getElementById("score-histogram").getContext('2d');
    var dataValues = [];
    var dataLabels = [];
    var chart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: dataLabels,
            datasets: [{
            label: 'Approx. Samples By Score',
            data: dataValues,
            backgroundColor: 'rgb(75, 192, 192)',
            }]
        },
        options: {
            scales: {
            xAxes: [{
                display: false,
                barPercentage: 1.2
            }, {
                display: true,
                ticks: {
                    autoSkip: true,
                    // max: 4,
                }
            }]
            }
        }
    });
        
        

    $('#measure-select select').on('change', function() {
        displayMeasureStats(this.value, chart);
    })

    waitForSelectedValue('#measure-select select', function(initSelected) {
        displayMeasureStats(initSelected, chart);
    })
    
})(jQuery);



displayScorePercentile = function(measure_id) {
    if (measure_id !== undefined) {
        $.ajax({
            url: prefixUrl + "/getScorePercentileByMeasure",
            data: {"measure_id": measure_id},
            dataType: "json",
            type: 'GET',
            success: function (response) {
                const data = response.data[0]

                const percentile = Object.keys(data).map(key => {
                    return { "name": key, "value": data[key].toFixed(3) };
                });
                
                $('#score-stats').DataTable( {
                    data: percentile,
                    lengthChange: false,
                    pageLength : 13,
                    info: false,
                    paging: false,
                    bDestroy: true,
                    order: [],
                    searching: false,
                    columns: [
                        { data: 'name', title: 'Name' },
                        { data: 'value', title: 'Value' }
                    ]
                });
            }
        })

        const limit = 5
        $.ajax({
            url: prefixUrl + "/getTopHospitalsByMeasure",
            data: {"measure_id": measure_id, "limit": limit},
            dataType: "json",
            type: 'GET',
            success: function (response) {
                const data = response.data.sort((a, b) => b.score - a.score)
                const highestScore = data.slice(0, limit)
                const lowestScore = data.slice(-limit)
                
                $('#highest-score-hospitals').DataTable( {
                    data: highestScore,
                    lengthChange: false,
                    pageLength : limit,
                    info: false,
                    paging: false,
                    bDestroy: true,
                    order: [],
                    searching: false,
                    columns: [
                        { data: 'facility_name', title: 'Hospital Name' },
                        { data: 'score', title: 'Score' },
                        { data: 'sample', title: 'Approx. Sample' }
                    ]
                });

                $('#lowest-score-hospital').DataTable( {
                    data: lowestScore,
                    lengthChange: false,
                    pageLength : limit,
                    info: false,
                    paging: false,
                    bDestroy: true,
                    order: [],
                    searching: false,
                    columns: [
                        { data: 'facility_name', title: 'Hospital Name' },
                        { data: 'score', title: 'Score' },
                        { data: 'sample', title: 'Approx. Sample' }
                    ]
                });
            }
        })
    }
};


(function($) {
    $('#measure-select select').on('change', function() {
        displayScorePercentile(this.value);
    })

    waitForSelectedValue('#measure-select select', function(initSelected) {
        displayScorePercentile(initSelected);
    })
    
})(jQuery);




// (function($) {
//     "use strict"
    

//     //todo list
//     $(".tdl-new").on('keypress', function(e) {

//         var code = (e.keyCode ? e.keyCode : e.which);

//         if (code == 13) {

//             var v = $(this).val();

//             var s = v.replace(/ +?/g, '');

//             if (s == "") {

//                 return false;

//             } else {

//                 $(".tdl-content ul").append("<li><label><input type='checkbox'><i></i><span>" + v + "</span><a href='#' class='ti-trash'></a></label></li>");

//                 $(this).val("");

//             }

//         }

//     });





//     $(".tdl-content a").on("click", function() {

//         var _li = $(this).parent().parent("li");

//         _li.addClass("remove").stop().delay(100).slideUp("fast", function() {

//             _li.remove();

//         });

//         return false;

//     });



//     // for dynamically created a tags

//     $(".tdl-content").on('click', "a", function() {

//         var _li = $(this).parent().parent("li");

//         _li.addClass("remove").stop().delay(100).slideUp("fast", function() {

//             _li.remove();

//         });

//         return false;

//     });








// })(jQuery);


// (function($) {
//     "use strict"

//     var i = new Datamap( {
//         scope: "world", 
//         element: document.getElementById("world-map"), 
//         responsive: !0, 
//         geographyConfig: {
//             popupOnHover: !1, 
//             highlightOnHover: !1, 
//             borderColor: "transparent", 
//             borderWidth: 1, 
//             highlightBorderWidth: 3, 
//             highlightFillColor: "rgba(0,123,255,0.5)", 
//             highlightBorderColor: "transparent", 
//             borderWidth: 1
//         }, 
//         bubblesConfig: {
//             popupTemplate: function (e, i) {
//                 return '<div class="datamap-sales-hover-tooltip">' + i.country + '<span class="ml-2"></span>' + i.sold + "</div>"
//             }, 
//             borderWidth: 0, 
//             highlightBorderWidth: 3, 
//             highlightFillColor: "rgba(0,123,255,0.5)", 
//             highlightBorderColor: "transparent", 
//             fillOpacity: .75
//         }, 
//         fills: {
//             Visited: "#777", 
//             neato: "#777", 
//             white: "#777", 
//             defaultFill: "#EBEFF2"
//         }
//     });
    
//     i.bubbles([{
//         centered: "USA", fillKey: "white", radius: 5, sold: "$500", country: "United States"
//     }, {
//         centered: "SAU", fillKey: "Visited", radius: 5, sold: "$900", country: "Saudia Arabia"
//     }, {
//         centered: "RUS", fillKey: "neato", radius: 5, sold: "$250", country: "Russia"
//     }, {
//         centered: "CAN", fillKey: "white", radius: 5, sold: "$1000", country: "Canada"
//     }, {
//         centered: "IND", fillKey: "Visited", radius: 5, sold: "$50", country: "India"
//     }, {
//         centered: "AUS", fillKey: "white", radius: 5, sold: "$700", country: "Australia"
//     }, {
//         centered: "BGD", fillKey: "Visited", radius: 5, sold: "$1500", country: "Bangladesh"
//     }
//     ]),
//     window.addEventListener("resize", function (e) {
//         i.resize()
//     });





// })(jQuery);



// (function($) {
//     "use strict"


//     $('#todo_list').slimscroll({
//         position: "right",
//         size: "5px",
//         height: "250px",
//         color: "transparent"
//     });

//     $('#activity').slimscroll({
//         position: "right",
//         size: "5px",
//         height: "390px",
//         color: "transparent"
//     });





// })(jQuery);



// (function($) {
//     "use strict"

//     let ctx = document.getElementById("chart_widget_2");
//     ctx.height = 280;
//     new Chart(ctx, {
//         type: 'line',
//         data: {
//             labels: ["2010", "2011", "2012", "2013", "2014", "2015", "2016"],
//             type: 'line',
//             defaultFontFamily: 'Montserrat',
//             datasets: [{
//                 data: [0, 15, 57, 12, 85, 10, 50],
//                 label: "iPhone X",
//                 backgroundColor: '#847DFA',
//                 borderColor: '#847DFA',
//                 borderWidth: 0.5,
//                 pointStyle: 'circle',
//                 pointRadius: 5,
//                 pointBorderColor: 'transparent',
//                 pointBackgroundColor: '#847DFA',
//             }, {
//                 label: "Pixel 2",
//                 data: [0, 30, 5, 53, 15, 55, 0],
//                 backgroundColor: '#F196B0',
//                 borderColor: '#F196B0',
//                 borderWidth: 0.5,
//                 pointStyle: 'circle',
//                 pointRadius: 5,
//                 pointBorderColor: 'transparent',
//                 pointBackgroundColor: '#F196B0',
//             }]
//         },
//         options: {
//             responsive: !0,
//             maintainAspectRatio: false,
//             tooltips: {
//                 mode: 'index',
//                 titleFontSize: 12,
//                 titleFontColor: '#000',
//                 bodyFontColor: '#000',
//                 backgroundColor: '#fff',
//                 titleFontFamily: 'Montserrat',
//                 bodyFontFamily: 'Montserrat',
//                 cornerRadius: 3,
//                 intersect: false,
//             },
//             legend: {
//                 display: false,
//                 position: 'top',
//                 labels: {
//                     usePointStyle: true,
//                     fontFamily: 'Montserrat',
//                 },


//             },
//             scales: {
//                 xAxes: [{
//                     display: false,
//                     gridLines: {
//                         display: false,
//                         drawBorder: false
//                     },
//                     scaleLabel: {
//                         display: false,
//                         labelString: 'Month'
//                     }
//                 }],
//                 yAxes: [{
//                     display: false,
//                     gridLines: {
//                         display: false,
//                         drawBorder: false
//                     },
//                     scaleLabel: {
//                         display: true,
//                         labelString: 'Value'
//                     }
//                 }]
//             },
//             title: {
//                 display: false,
//             }
//         }
//     });


    


// })(jQuery);

// (function($) {
//     "use strict"

//     let ctx = document.getElementById("chart_widget_3");
//     ctx.height = 130;
//     new Chart(ctx, {
//         type: 'line',
//         data: {
//             labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun"],
//             type: 'line',
//             defaultFontFamily: 'Montserrat',
//             datasets: [{
//                 data: [0, 15, 57, 12, 85, 10],
//                 label: "iPhone X",
//                 backgroundColor: 'transparent',
//                 borderColor: '#847DFA',
//                 borderWidth: 2,
//                 pointStyle: 'circle',
//                 pointRadius: 5,
//                 pointBorderColor: '#847DFA',
//                 pointBackgroundColor: '#fff',
//             }]
//         },
//         options: {
//             responsive: !0,
//             maintainAspectRatio: true,
//             tooltips: {
//                 mode: 'index',
//                 titleFontSize: 12,
//                 titleFontColor: '#fff',
//                 bodyFontColor: '#fff',
//                 backgroundColor: '#000',
//                 titleFontFamily: 'Montserrat',
//                 bodyFontFamily: 'Montserrat',
//                 cornerRadius: 3,
//                 intersect: false,
//             },
//             legend: {
//                 display: false,
//                 position: 'top',
//                 labels: {
//                     usePointStyle: true,
//                     fontFamily: 'Montserrat',
//                 },


//             },
//             scales: {
//                 xAxes: [{
//                     display: false,
//                     gridLines: {
//                         display: false,
//                         drawBorder: false
//                     },
//                     scaleLabel: {
//                         display: false,
//                         labelString: 'Month'
//                     }
//                 }],
//                 yAxes: [{
//                     display: false,
//                     gridLines: {
//                         display: false,
//                         drawBorder: false
//                     },
//                     scaleLabel: {
//                         display: true,
//                         labelString: 'Value'
//                     }
//                 }]
//             },
//             title: {
//                 display: false,
//             }
//         }
//     });


    


// })(jQuery);



/*******************
Chart Chartist
*******************/
// (function($) {
//     "use strict"


//     new Chartist.Line("#chart_widget_3", {
//         labels: ["1", "2", "3", "4", "5", "6", "7", "8"],
//         series: [
//             [4, 5, 1.5, 6, 7, 5.5, 5.8, 4.6]
//         ]
//     }, {
//         low: 0,
//         showArea: !1,
//         showPoint: !0,
//         showLine: !0,
//         fullWidth: !0,
//         lineSmooth: !1,
//         chartPadding: {
//             top: 4,
//             right: 4,
//             bottom: -20,
//             left: 4
//         },
//         axisX: {
//             showLabel: !1,
//             showGrid: !1,
//             offset: 0
//         },
//         axisY: {
//             showLabel: !1,
//             showGrid: !1,
//             offset: 0
//         }
//     });


//     new Chartist.Pie("#chart_widget_3_1", {
//         series: [35, 65]
//     }, {
//         donut: !0,
//         donutWidth: 10,
//         startAngle: 0,
//         showLabel: !1
//     });

// })(jQuery);




/*******************
Pignose Calender
*******************/
// (function ($) {
//     "use strict";

//     $(".year-calendar").pignoseCalendar({
//         theme: "blue"
//     }), $("input.calendar").pignoseCalendar({
//         format: "YYYY-MM-DD"
//     });

// })(jQuery);