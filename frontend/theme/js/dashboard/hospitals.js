(function($) {
    const states = ["AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", 
        "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", 
        "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", 
        "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", 
        "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"
    ];
    $.each(states, function(i, value) {
        $('#state-select select').append(
            '<option value="' + value + '">' + value + '</option>'
        )
    })
})(jQuery);



(function($) {
    $("#search-hospital-button").click(function(){
        document.getElementById('loading-icon').style.display = 'block';
        $("#search-hospital-button").prop("disabled", true)

        var hospitalName = $("#hospital-name").val();
        var hospitalCity = $("#city-county").val();
        var hospitalZipcode = $("#zipcode").val();
        var hopitalState = $("#state-select select").val();

        var params = {
            "state": hopitalState,
            "zipcode": hospitalZipcode,
            "city_county": hospitalCity,
            "name": hospitalName
        }

        $.ajax({
            url: prefixUrl + "/getHospitals",
            data: params,
            dataType: "json",
            type: 'GET',
            success: function (response) {
                const data = response.data
                
                console.log(data)
                
                document.getElementById('loading-icon').style.display = 'none';
                $("#search-hospital-button").prop("disabled", false)

                var hospitalTable = $('#hospitals').DataTable( {
                    data: data,
                    bDestroy: true,
                    columns: [
                        {
                            "className": 'details-control',
                            "orderable": false,
                            "data": null,
                            "defaultContent": '<i class="fa fa-plus"></i>'
                        },
                        { data: 'facility_name', title: 'Hospital Name' },
                        { data: 'address', title: 'Address' },
                        { data: 'city_town', title: 'City/Town' },
                        { data: 'county_parish', title: 'County/Parish' },
                        { data: 'state', title: 'State' },
                        { data: 'zip_code', title: 'Zipcode'},
                        { data: 'telephone_number', title: 'Telephone Number'}
                    ]
                });

                $('#hospitals tbody').on('click', 'td.details-control', function () {
                    var tr = $(this).closest('tr');
                    var row = hospitalTable.row(tr);
            
                    if (row.child.isShown()) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    }
                    else {
                        // // Open this row
                        // row.child(format(row.data())).show();
                        // tr.addClass('shown');

                        var rowData = row.data();
                        var hospitalId = rowData.facility_id; 
                        
                        console.log(hospitalId)
                        // row.child(format(row.data())).show();
                        $.ajax({
                            url: prefixUrl + "/getHospitalScores", 
                            method: 'GET', 
                            data: {"facility_id": hospitalId},
                            success: function(response) {
                                console.log(response.data)
                                row.child(format(response.data[0])).show();
                                tr.addClass('shown');
                            },
                            error: function() {
                                // Handle error
                                console.log('Error fetching details');
                            }
                        });
                    }
                });
            
                function format(d) {
                    delete d['facility_id']
                    var keys = Object.keys(d);

                    // Start building the HTML for the table
                    var html = '<table>';

                    // Create table headers in steps of 5
                    for (var i = 0; i < keys.length; i += 5) {
                        html += '<thead><tr>';
                        for (var j = i; j < i + 5 && j < keys.length; j++) {
                            html += '<th>' + keys[j] + ':\t' + d[keys[j]] + '</th>';
                        }
                        html += '</tr></thead>';
                    }

                    html += '</tbody></table>';
                    return html;
                }
                
            },
            // error: function() {
            //     document.getElementById('loading-icon').style.display = 'none';
            // }
        });
    });

})(jQuery);