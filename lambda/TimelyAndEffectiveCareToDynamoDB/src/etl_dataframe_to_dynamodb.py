from functools import partial

import pandas as pd

from DataQuery.Config import *
from DataQuery.DynamoDBStore import DynamoDBStore

dynamodb_store = DynamoDBStore(DynamoDB.instance())
s3 = S3.instance()


def etl_dataframe_to_dynamodb(func, target_table):
    def etl():
        try:
            df: pd.DataFrame = func()
            items = df.to_dict(orient='records')
            dynamodb_store.write_many_items(
                table_name=target_table,
                items=items
            )
        except:
            print(f"Executed {func.__name__} Failed")

    return etl


# @partial(etl_dataframe_to_dynamodb, target_table="timely_and_effective_care_hospitals")
# def etl_hospital_data() -> pd.DataFrame:
#     """
#     get data of hospitals and load into dynamodb
#     :return:
#     """
#     query_string = f"""
#         select
#             distinct
#                 facility_id, facility_name, address, city_town,
#                 state, zip_code, county_parish, telephone_number
#         from {database}.timely_and_effective_care_hospital
#     """
#     return data_catalog.query(database=database, query_string=query_string, query_output=athena_query_output)


@partial(etl_dataframe_to_dynamodb, target_table="timely_and_effective_hospital_scores")
def etl_hospital_score() -> pd.DataFrame:
    """
    get pivoted scores of hospitals
    :return:
    """
    bucket_name = 'cms-data-for-app'
    prefix = 'timely_and_effective_care_hospital/star_schema_transformed/timely_and_effective_care_fact/'

    response = s3.list_objects_v2(Bucket=bucket_name, Prefix=prefix)
    dfs = []

    if response.get('Contents'):
        for item in response['Contents']:
            key = item['Key']
            if key.endswith('.parquet'):
                df = pd.read_parquet(f"s3://{bucket_name}/{key}")
                dfs.append(df)
    df = pd.concat(dfs)
    df_pivot = df.pivot(index='facility_id', columns='measure_id')['score'].reset_index()

    return df_pivot
