from functools import partial

from DataQuery.Config import *
from DataQuery.DataCatalog import DataCatalog
from DataQuery.DynamoDBStore import DynamoDBStore
from DataQuery.QueryResult import QueryResult

dynamodb_store = DynamoDBStore(DynamoDB.instance())
data_catalog = DataCatalog(Athena.instance())

database = "cms_for_app"
table = "timely_and_effective_care_hospital"
athena_query_output = f"s3://cms-data-for-app/{table}/query_data/"


def etl_athena_to_dynamodb(func, target_table):
    def etl():
        query_data = func()
        if query_data.status == Query_Succeed:
            dynamodb_store.write_many_items(
                table_name=target_table,
                items=query_data.data
            )
        else:
            print(f"Executed {func.__name__} Failed")

    return etl


@partial(etl_athena_to_dynamodb, target_table="timely_and_effective_care_measure")
def etl_measure_data() -> QueryResult:
    """
    get data of measure and load into dynamodb
    :return:
    """
    query_string = f"""
        select
            distinct measure_id, measure_name
        from {database}.timely_and_effective_care_hospital
    """
    return data_catalog.query(database=database, query_string=query_string, query_output=athena_query_output)


@partial(etl_athena_to_dynamodb, target_table="timely_and_effective_care_summary")
def etl_summary_data() -> QueryResult:
    """
    get summary of data
    :return:
    """
    query_string = f"""
        select
            'all' as facility_id,
            sum(
                CASE 
                    WHEN sample != 'Not Available' AND sample is not NULL THEN cast(sample as INTEGER)
                    WHEN footnote like '%,%' THEN cast(SPLIT(footnote, ',')[1] as INTEGER)
                    WHEN footnote != 'Not Available' AND footnote is not NULL THEN cast(footnote as INTEGER)
                    ELSE 0
                END
            ) as total_cares,
            count(distinct facility_id) as total_hospitals,
            count(distinct condition) as total_condition,
            count(distinct measure_id) as total_measures,
            min(date(date_parse(start_date, '%m/%d/%Y'))) as period_start,
            max(date(date_parse(start_date, '%m/%d/%Y'))) as period_end
        from {database}.timely_and_effective_care_hospital
    """
    return data_catalog.query(database=database, query_string=query_string, query_output=athena_query_output)