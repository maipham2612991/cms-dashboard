import json

from etl_dataframe_to_dynamodb import *
from etl_athena_to_dynamodb import *


def lambda_handler(event, context):
    # get data of measure and load into dynamodb

    # etl_hospital_data()
    # etl_measure_data()
    # etl_summary_data()
    etl_hospital_score()

    return {
        'statusCode': 200,
        'body': json.dumps('Done running')
    }
