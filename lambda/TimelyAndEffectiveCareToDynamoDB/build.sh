cd ../
docker build --platform linux/amd64 -t cms-etl-dynamodb:s3-to-dynamodb -f TimelyAndEffectiveCareToDynamoDB/Dockerfile .

# or docker build --platform linux/amd64 -t cms-etl-dynamodb:s3-to-dynamodb .
docker run --platform linux/amd64 -p 9000:8080 cms-etl-dynamodb:s3-to-dynamodb
docker tag cms-etl-dynamodb:s3-to-dynamodb 676635927244.dkr.ecr.us-east-1.amazonaws.com/cms-etl-dynamodb:latest

docker images -a | grep none | awk '{ print $3; }' | xargs docker rmi -f # prune images
docker push 676635927244.dkr.ecr.us-east-1.amazonaws.com/cms-etl-dynamodb:latest