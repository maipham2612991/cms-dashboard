import requests
import boto3


def lambda_handler(event, context):
    url = 'https://data.cms.gov/provider-data/api/1/metastore/schemas/dataset/items/yv7e-xc69'
    params = {
        'show-reference-ids': 'false'
    }
    headers = {
        'accept': 'application/json'
    }

    response = requests.get(url=url, params=params, headers=headers)
    download_url = response.json()['distribution'][0]['data']['downloadURL']

    s3 = boto3.client('s3')
    bucket = 'cms-data-for-app'
    key = 'timely_and_effective_care_hospital.csv'

    content = requests.get(url=download_url).content
    s3.put_object(Bucket=bucket, Key=key, Body=content)
