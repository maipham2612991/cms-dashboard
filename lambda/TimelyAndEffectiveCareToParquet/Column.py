from enum import Enum


class Column:
    def __init__(self, name, data_type, to_value_format=None):
        self.name = name
        self.data_type = data_type
        self.value_format = to_value_format


class DataType(Enum):
    Date = 'date'
    String = 'string'
    Int = 'int'

