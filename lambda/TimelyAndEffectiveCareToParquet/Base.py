from datetime import datetime
from io import BytesIO
from typing import List

import pandas as pd

from Column import Column, DataType


class Base:
    def __init__(self, table_name):
        self.table_name = table_name
        self.columns: List[Column] = []
        self.custom_function = {}
        self.data = None

    def add_column(self, column: Column):
        self.columns.append(column)
        return self

    def apply(self, col_name, func):
        for col in self.columns:
            if col.name == col_name:
                self.custom_function[col_name] = func
                return self

        raise print(f"Column name {col_name} not found")

    def build_data(self, df):
        if self.data is None:
            self.data = df[[col.name for col in self.columns]].drop_duplicates()

            # apply function
            for col_name, func in self.custom_function.items():
                self.data[col_name] = self.data[col_name].apply(func)

            # cast type
            for column in self.columns:
                if column.data_type == DataType.Date and column.value_format:
                    self.data[column.name] = pd.to_datetime(self.data[column.name]).dt.strftime(column.value_format)
                elif column.data_type == DataType.Int:
                    self.data[column.name] = pd.to_numeric(self.data[column.name], errors='coerce').astype('Int64')
        else:
            raise print("Data is already loaded")

        return self

    def write_parquet_s3(self, s3, bucket, prefix):
        current_date = datetime.now().strftime("%Y/%m/%d")
        df_buffer = BytesIO()
        self.data.to_parquet(df_buffer, index=False)
        s3.put_object(
            Bucket=bucket,
            Key=f"{prefix}/{self.table_name}/{current_date}/data.parquet",
            Body=df_buffer.getvalue()
        )
