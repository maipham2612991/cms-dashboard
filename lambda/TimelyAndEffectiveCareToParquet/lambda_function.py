import boto3
import pandas as pd

from Base import Base
from Column import Column, DataType


def lambda_handler(event, context):
    s3 = boto3.client('s3')
    bucket = "cms-data-for-app"
    prefix = "timely_and_effective_care_hospital"
    read_key = f"{prefix}/input/timely_and_effective_care_hospital.csv"

    response = s3.get_object(Bucket=bucket, Key=read_key)

    status = response.get("ResponseMetadata", {}).get("HTTPStatusCode")
    if status == 200:
        print(f"Successful S3 get_object response. Status - {status}")
        df = pd.read_csv(response.get("Body"))
        df.rename(columns={
            'Facility ID': 'facility_id',
            'Facility Name': 'facility_name',
            'Address': 'address',
            'City/Town': 'city_town',
            'State': 'state',
            'ZIP Code': 'zip_code',
            'County/Parish': 'county_parish',
            'Telephone Number': 'telephone_number',
            'Condition': 'condition',
            'Measure ID': 'measure_id',
            'Measure Name': 'measure_name',
            'Score': 'score',
            'Sample': 'sample',
            'Footnote': 'footnote',
            'Start Date': 'start_date',
            'End Date': 'end_date'
        }, inplace=True)
        df.replace('Not Available', None, inplace=True)

        transformed_location = f"{prefix}/star_schema_transformed"

        """Star Schema: Hospital"""
        Base(table_name='hospital_dimension')\
            .add_column(Column(name='facility_id', data_type=DataType.String)) \
            .add_column(Column(name='facility_name', data_type=DataType.String)) \
            .add_column(Column(name='address', data_type=DataType.String)) \
            .add_column(Column(name='city_town', data_type=DataType.String)) \
            .add_column(Column(name='state', data_type=DataType.String)) \
            .add_column(Column(name='zip_code', data_type=DataType.Int)) \
            .add_column(Column(name='county_parish', data_type=DataType.String)) \
            .add_column(Column(name='telephone_number', data_type=DataType.String)) \
            .build_data(df) \
            .write_parquet_s3(s3=s3, bucket=bucket, prefix=transformed_location)
        print(f"Successful put_object hospital_dimension")

        """Star Schema: Condition_Measure"""
        Base(table_name='measure_dimension') \
            .add_column(Column(name='measure_id', data_type=DataType.String)) \
            .add_column(Column(name='condition', data_type=DataType.String)) \
            .add_column(Column(name='measure_name', data_type=DataType.String)) \
            .build_data(df) \
            .write_parquet_s3(s3=s3, bucket=bucket, prefix=transformed_location)
        print(f"Successful put_object measure_dimension")

        """Star Schema: Fact"""
        Base(table_name='timely_and_effective_care_fact') \
            .add_column(Column(name='facility_id', data_type=DataType.String)) \
            .add_column(Column(name='measure_id', data_type=DataType.String)) \
            .add_column(Column(name='start_date', data_type=DataType.Date, to_value_format='%Y-%m-%d')) \
            .add_column(Column(name='end_date', data_type=DataType.Date, to_value_format='%Y-%m-%d')) \
            .add_column(Column(name='score', data_type=DataType.String)) \
            .add_column(Column(name='sample', data_type=DataType.Int)) \
            .add_column(Column(name='footnote', data_type=DataType.Int)) \
            .apply(col_name='footnote', func=lambda x: str(x).split(',')[0] if x else x) \
            .build_data(df) \
            .write_parquet_s3(s3=s3, bucket=bucket, prefix=transformed_location)
        print(f"Successful put_object timely_and_effective_care_fact")

        return {
            'statusCode': 'Successfully transformed data'
        }
    else:
        return {
            'statusCode': f"Unsuccessful S3 get_object response. Status - {status}",
        }
