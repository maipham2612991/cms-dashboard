# cms-dashboard



## Getting started

This project aimed for displaying healthcare data dashboard leveraging the AWS serverless architecture and cms data.
Then, the total expense for this project is 0 and it can also be deployed in free tier.

## Architecture

![alt text](./cms_architecture.png)

## Output

The dashboard result is shown in https://d1ux3ufcwoooy2.cloudfront.net/
