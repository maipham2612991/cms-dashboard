import json
from .APIHandlers import *


API_V1 = '/api/v1/timely_and_effective_care'


def lambda_handler(event, context):
    """Sample pure Lambda function

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    ------
    API Gateway Lambda Proxy Output Format: dict

        Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """

    resource = event['resource']
    params = event['queryStringParameters']
    handler_mapping = {
        f"{API_V1}/test": test_availability,
        f"{API_V1}/summary": summary,
        f"{API_V1}/getSamplesByCondition": get_samples_by_condition,
        f"{API_V1}/getSamplesByMeasure": get_samples_by_measure,
        f"{API_V1}/getScoreStatsByMeasure": get_score_stats_by_measure,
        f"{API_V1}/getScorePercentileByMeasure": get_score_percentile_by_measure,
        f"{API_V1}/getTopHospitalsByMeasure": get_top_hospitals_by_measure,
        f"{API_V1}/getHospitals": get_hospitals,
        f"{API_V1}/getHospitalScores": get_hospital_scores,
    }

    message = handler_mapping[resource](params)

    return {
        'statusCode': 200,
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
        'body': json.dumps(message)
    }
