import boto3

ATHENA_REUSE_TIME = 10080
DATE_FORMAT = "%Y/%m/%d"
Query_Succeed = "SUCCEED"
Query_Failed = "FAILED"


class Athena:
    athena = None

    @classmethod
    def instance(cls):
        if cls.athena is None:
            cls.athena = boto3.client('athena')
        return cls.athena


class DynamoDB:
    dynamodb = None

    @classmethod
    def instance(cls):
        if cls.dynamodb is None:
            cls.dynamodb = boto3.resource('dynamodb')
        return cls.dynamodb


class S3:
    s3 = None

    @classmethod
    def instance(cls):
        if cls.s3 is None:
            cls.s3 = boto3.client('s3')
        return cls.s3
