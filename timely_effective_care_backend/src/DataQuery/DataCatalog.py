from .Config import *
import time
from .DataStore import DataStore
from .QueryResult import QueryResult, Column


class DataCatalog(DataStore):
    def __init__(self, athena):
        self.athena = athena

    def query(self, database, query_string, query_output) -> QueryResult:
        # start_query_execution
        start_response = self.athena.start_query_execution(
            QueryString=query_string,
            QueryExecutionContext={
                'Database': database
            },
            ResultConfiguration={
                'OutputLocation': query_output,
            },
            ResultReuseConfiguration={
                'ResultReuseByAgeConfiguration': {
                    'Enabled': True,
                    'MaxAgeInMinutes': ATHENA_REUSE_TIME
                }
            }
        )

        # get_query_execution until it done
        query_execution_id = start_response['QueryExecutionId']
        while True:
            running_response = self.athena.get_query_execution(
                QueryExecutionId=query_execution_id
            )
            if running_response['QueryExecution']['Status']['State'] not in ['QUEUED', 'RUNNING', None]:
                break
            time.sleep(0.1)

        # Get result of query
        if running_response['QueryExecution']['Status']['State'] in ['FAILED', 'CANCELLED']:
            return QueryResult(status=Query_Failed)

        result_response = self.athena.get_query_results(
            QueryExecutionId=query_execution_id
        )

        # get result response
        query_result = QueryResult(Query_Succeed)
        result_columns = result_response['ResultSet']['ResultSetMetadata']['ColumnInfo']
        result_rows = result_response['ResultSet']['Rows']
        for column in result_columns:
            query_result.add_column(Column(column['Name'], column['Type']))

        for row in result_rows[1:]: # exclude header
            values = [item.get('VarCharValue') for item in row['Data']]
            query_result.add_row(values)

        return query_result



