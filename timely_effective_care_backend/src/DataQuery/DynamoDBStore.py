from botocore.exceptions import ClientError

from .Config import *
from .DataStore import DataStore
from .QueryResult import QueryResult, Column


class DynamoDBStore(DataStore):
    def __init__(self, dynamodb):
        self.dynamodb = dynamodb

    def query(self, table, query_string, query_output):
        pass

    def get_item(self, table_name, key: dict) -> QueryResult:
        table = self.dynamodb.Table(table_name)

        try:
            response = table.get_item(Key=key)
        except ClientError as err:
            print(
                f"Couldn't get data from table {table_name}. "
                f"Here's why: {err.response['Error']['Code']}: {err.response['Error']['Message']}"
            )
            return QueryResult(status=Query_Failed).to_dict()
        else:
            query_result = QueryResult(status=Query_Succeed)
            item = response["Item"]

            for key, value in item.items():
                query_result.add_column(Column(name=key,datatype=value.__class__.__name__))
            query_result.add_row(item.values())

            return query_result

    # def scan(self, table):
    #     table = self.dynamodb.Table(table)
    #     response = table.scan()

    def write_many_items(self, table_name, items):
        print(f"Writing {len(items)} to Dynamodb table {table_name}")
        for i in range(0, len(items), 25):
            batch = []
            for item in items[i:i + 25]:
                for key, value in item.items():
                    if value is not None and isinstance(value, float):
                        item[key] = str(value)
                batch.append({'PutRequest': {'Item': item}})

            try:
                self.dynamodb.batch_write_item(
                    RequestItems={table_name: batch}
                )
                print(f"Successfully write items on batch {i}")
            except Exception as e:
                print(f"Error writing batch: {e}")
