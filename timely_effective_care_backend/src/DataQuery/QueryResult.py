import json


class Column:
    def __init__(self, name, datatype):
        self.name = name
        self.type = datatype


class QueryResult:
    def __init__(self, status):
        self.status = status
        self.columns: list[Column] = []
        self.data = []

    def add_column(self, column: Column):
        self.columns.append(column)
        return self

    def add_row(self, values: list):
        item = {}
        try:
            for col, value in zip(self.columns, values):
                if col.type in ["double", "float"]:
                    item[col.name] = float(value)
                elif col.type in ['int', 'bigint', 'smallint', 'tinyint', 'Decimal']:
                    item[col.name] = int(value)
                elif col.type in ['boolean']:
                    item[col.name] = bool(value)
                elif col.type in ['array', 'struct']:
                    item[col.name] = json.loads(value)
                else:
                    item[col.name] = value

            self.data.append(item)

        except:
            print("Number of rows/columns mismatched")

    def to_dict(self):
        return {
            'queryStatus': self.status,
            'data': self.data
        }
