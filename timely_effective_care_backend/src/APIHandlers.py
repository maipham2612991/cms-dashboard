from .DataQuery.Config import *
from .DataQuery.DataCatalog import DataCatalog
from .DataQuery.DynamoDBStore import DynamoDBStore

DATABASE = "cms_for_app"
TABLE = "timely_and_effective_care_hospital"
ATHENA_QUERY_OUTPUT = "s3://cms-data-for-app/timely_and_effective_care_hospital/query_data/"
measure_table = "measure_dimension"
hospital_table = "hospital_dimension"
fact_table = "timely_and_effective_care_fact"


def test_availability(params):
    return {
        "success": True,
        "message": "Service available"
    }


def summary(params):
    table_name = 'timely_and_effective_care_summary'

    return DynamoDBStore(dynamodb=DynamoDB.instance()) \
        .get_item(table_name=table_name, key={'facility_id': 'all'}) \
        .to_dict()


def get_hospital_scores(params):
    hospital_id = params['facility_id']
    table_name = 'timely_and_effective_hospital_scores'

    return DynamoDBStore(dynamodb=DynamoDB.instance()) \
        .get_item(table_name=table_name, key={'facility_id': hospital_id}) \
        .to_dict()


def get_hospitals(params):
    state = params['state']
    zipcode = params['zipcode']
    city_county: str = params['city_county'].upper()
    name = params['name'].upper()

    filter_zipcode = f"AND zip_code = {zipcode}" if zipcode else ""
    filter_city_county = f"AND (city_town like '%{city_county}%' or county_parish like '%{city_county}%') " if city_county else ""
    filter_name = f"AND facility_name = '%{name}%'" if name else ""

    query = f"""
        SELECT
            *
        FROM {DATABASE}.{hospital_table}
        WHERE state = '{state}'
        {filter_zipcode}
        {filter_city_county}
        {filter_name}
    """

    return DataCatalog(athena=Athena.instance()) \
        .query(database=DATABASE, query_string=query, query_output=ATHENA_QUERY_OUTPUT) \
        .to_dict()


def get_score_stats_by_measure(params):
    measure_id = params['measure_id']
    query = f"""
        select
            ROUND(CAST(score as double)) as score,
            sum(COALESCE(sample, footnote, 0)) as total_samples
        from {DATABASE}.{fact_table}
        where measure_id = '{measure_id}'
        and score is not null and score not in ('high', 'low', 'medium', 'very high')
        group by 1
    """

    return DataCatalog(athena=Athena.instance()) \
        .query(database=DATABASE, query_string=query, query_output=ATHENA_QUERY_OUTPUT) \
        .to_dict()


def get_score_percentile_by_measure(params):
    measure_id = params['measure_id']
    query = f"""
        select
            count(score) as count,
            avg(CAST(score as double)) as average,
            min(CAST(score as double)) as min,
            approx_percentile(CAST(score as double), 0.01) as "1%",
            approx_percentile(CAST(score as double), 0.05) as "5%",
            approx_percentile(CAST(score as double), 0.1) as "10%",
            approx_percentile(CAST(score as double), 0.25) as "25%",
            approx_percentile(CAST(score as double), 0.50) as "50%",
            approx_percentile(CAST(score as double), 0.75) as "75%",
            approx_percentile(CAST(score as double), 0.90) as "90%",
            approx_percentile(CAST(score as double), 0.95) as "95%",
            approx_percentile(CAST(score as double), 0.99) as "99%",
            max(CAST(score as double)) as max
        from {DATABASE}.{fact_table}
        where measure_id = '{measure_id}'
        and score is not null and score not in ('high', 'low', 'medium', 'very high')
    """

    return DataCatalog(athena=Athena.instance()) \
        .query(database=DATABASE, query_string=query, query_output=ATHENA_QUERY_OUTPUT) \
        .to_dict()


def get_top_hospitals_by_measure(params):
    limit = params['limit']
    measure_id = params['measure_id']
    query = f"""
        select
            h.facility_name,
            tmp.score,
            sample
        from (
            (select
                facility_id,
                CAST(score as double) as score,
                sample
            from {DATABASE}.{fact_table}
            where measure_id = '{measure_id}'
            order by score desc
            limit {limit})
            union all
            (select
                facility_id,
                CAST(score as double) as score,
                sample
            from {DATABASE}.{fact_table}
            where measure_id = '{measure_id}'
            order by score asc
            limit {limit})
        ) as tmp
        inner join {DATABASE}.{hospital_table} as h
        on tmp.facility_id = h.facility_id
    """

    return DataCatalog(athena=Athena.instance()) \
        .query(database=DATABASE, query_string=query, query_output=ATHENA_QUERY_OUTPUT) \
        .to_dict()


def get_samples_by_condition(params):
    query = f"""
        select
            measure.condition,
            count(distinct facility_id) as total_facilities,
            sum(COALESCE(sample, footnote, 0)) as total_samples,
            100.0 * sum(COALESCE(sample, footnote, 0))/
                (select sum(COALESCE(sample, footnote, 0)) from {DATABASE}.{fact_table}) as percentage
        from {DATABASE}.{fact_table} as fact
        inner join {DATABASE}.{measure_table} as measure
        on fact.measure_id = measure.measure_id
        group by 1
    """

    return DataCatalog(athena=Athena.instance()) \
        .query(database=DATABASE, query_string=query, query_output=ATHENA_QUERY_OUTPUT) \
        .to_dict()


def get_samples_by_measure(params):
    query = f"""
        select
            condition,
            measure.measure_id,
            measure.measure_name,
            total_samples,
            100.0 * total_samples/sum(total_samples) over(partition by condition) as percentage
        from (
            select
                measure_id,
                sum(COALESCE(sample, footnote, 0)) as total_samples
            from {DATABASE}.{fact_table}
            group by 1
        ) as fact
        inner join {DATABASE}.{measure_table} as measure
        on fact.measure_id = measure.measure_id
    """

    return DataCatalog(athena=Athena.instance()) \
        .query(database=DATABASE, query_string=query, query_output=ATHENA_QUERY_OUTPUT) \
        .to_dict()
