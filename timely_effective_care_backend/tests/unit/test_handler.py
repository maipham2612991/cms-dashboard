import json

import pytest

from timely_effective_care_backend.src import app


@pytest.fixture()
def apigw_event() -> dict:
    """ Generates API GW Event"""

    try:
        with open('./timely_effective_care_backend/events/test-event.json', 'r') as f:
            event = json.load(f)

            return event
    except Exception as e:
        raise e


def test_lambda_handler(apigw_event):

    ret = app.lambda_handler(apigw_event, "")
    expected = {
        "success": True,
        "message": "Service available"
    }
    expected_headers = {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
    }

    assert ret["statusCode"] == 200
    assert json.loads(ret["body"]) == expected
    assert ret["headers"] == expected_headers
