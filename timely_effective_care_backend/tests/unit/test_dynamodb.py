import os

import localstack_client.session as boto3
import pytest

from timely_effective_care_backend.src.DataQuery.DynamoDBStore import DynamoDBStore


@pytest.fixture()
def dynamodb_config():
    """
    Config AWS Localstack and DynamoDB
    :return: dynamodb client
    """

    # Create a DynamoDB client using boto3
    dynamodb = boto3.resource('dynamodb')

    # Define your table schema
    table_name = 'summary'
    table = dynamodb.create_table(
        TableName=table_name,
        KeySchema=[
            {
                'AttributeName': 'username',
                'KeyType': 'HASH'  # Partition key
            }
        ],
        AttributeDefinitions=[
            {
                'AttributeName': 'username',
                'AttributeType': 'S'
            }
        ],
        ProvisionedThroughput={
            'ReadCapacityUnits': 5,
            'WriteCapacityUnits': 5
        }
    )

    # Wait for the table to be created
    table.meta.client.get_waiter('table_exists').wait(TableName=table_name)

    print(f"Table {table_name} created")

    # Insert an item
    table.put_item(
        Item={
            'username': 'janedoe',
            'last_name': 'Doe',
            'age': 29,
            'account_type': 'standard_user',
        }
    )
    print("Item inserted")

    yield dynamodb

    table.delete()

    # Wait until the table is actually deleted
    table.meta.client.get_waiter('table_not_exists').wait(TableName=table_name)


def test_get_item_base(dynamodb_config):
    ret = DynamoDBStore(dynamodb=dynamodb_config) \
        .get_item(table_name='summary', key={'username': 'janedoe'}) \
        .to_dict()

    expected = {
        'username': 'janedoe',
        'last_name': 'Doe',
        'age': 29,
        'account_type': 'standard_user',
    }

    assert ret['data'][0] == expected





